import mongoose from 'mongoose';
import logger from '../lib/logger';
import config from '../config/config.dev';

mongoose.Promise = global.Promise;

const connectToDb = async () => {
    let dbHost = config.dbHost;
    let dbPort = config.dbPort;
    let dbName = config.dbName;
    try {
        await mongoose.connect(`mongodb://${dbHost}:${dbPort}/${dbName}`,
                       {'user': 'jteso',
                       'pass': 'cdujteso#100',
                        useMongoClient: true});
        logger.debug('Connected to mongo!!!');
    } catch (err) {
        logger.error('Could not connect to MongoDB due to error: ' + err.message);
    }
}

export default connectToDb;
