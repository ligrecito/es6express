import mongoose from 'mongoose';

const InitDb = () => {
	// connect to a database if needed, then pass it to `callback`:
	mongoose.connect('mongodb://localhost:27017/testdb', {
		useMongoClient: true
	});
};

export default InitDb;
