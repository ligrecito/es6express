import path from 'path';

let config = {};

config.logFileDir = path.join(__dirname, '../../log');
config.logFileName = 'app.log';
config.dbHost = process.env.dbHost || 'ds125198.mlab.com';
config.dbPort = process.env.dbPort || '25198';
config.dbName = process.env.dbName || 'cplane';
config.serverPort = process.env.serverPort || 8080;
config.corsHeaders = ["Link"];
config.bodyLimit = '1000kb';

// providers
config.providers = {
  googleUrl: 'http://35.189.19.195:8089/api/boral/k8/v1',
  kongUrl: 'http://localhost:8001/apis'
};


export default config;
