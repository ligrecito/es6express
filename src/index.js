import http from 'http';
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import mongoose from 'mongoose';
import jwt from 'jsonwebtoken'; // used to create, sign and verify tokens
import config from './config/config.dev'; // get our config file
import jwtValidator from './middleware/jwt-validation';
import logger from './lib/logger';
import authController from './sts/auth/controller';
import apiControllers from './api';
import connectToDb from './db/connect';
import {
  KongError,
  ValidationError,
  ResourceNotFoundError
} from './lib/custom-errors';

let app = express();
app.server = http.createServer(app);

// Injecting mongo db accessible via: `req.db`
logger.debug('Connecting to mongodb ...');
connectToDb();

// logger
app.use(morgan('dev'));

// 3rd party middleware
app.use(cors({ exposedHeaders: config.corsHeaders }));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// ** PUBLIC API **
app.use('/sts', authController);

// ** PROTECTED API **
// app.use(jwtValidator);
app.use('/api', apiControllers);

// Error handlers
app.use(function handleKongErrors(error, req, res, next) {
  if (error instanceof KongError) {
    return res.status(503).json({
      type: 'KongError',
      message: error.message
    });
  }
  next(error);
});
app.use(function handleValidationErrors(error, req, res, next) {
  if (error instanceof ValidationError) {
    return res.status(422).json({
      type: 'ValidationError',
      message: error.message
    });
  }
  next(error);
});
app.use(function handleResourceNotFoundErrors(error, req, res, next) {
  console.log('i am here!!!', error instanceof ResourceNotFoundError)
  if (error instanceof ResourceNotFoundError) {
    return res.status(404).json({
      type: 'ResourceNotFoundError',
      message: error.message
    });
  }
  next(error);
});



app.server.listen(process.env.PORT || config.serverPort, ()=> {
  console.log(`==> Magic happens at http://localhost:${app.server.address().port}`);
});

export default app;
