import mongoose from 'mongoose';
import bcrypt from 'bcrypt-nodejs';

const userSchema = new mongoose.Schema({
    username: { type: String, lowercase: true, unique: true, required: true, index: { unique: true } },
    password: { type: String, required: true },
    loginTries: Number,
    blocked: Boolean,
    isAdmin: Boolean,
    info: {
      name: String,
      email: String,
    },
    created_at: Date,
    updated_at: Date
});

userSchema.pre('save', function(next) {
    const user = this;

    bcrypt.genSalt(10, (err, salt) => {
        if (err) {
            return next(err);
        }

        bcrypt.hash(user.password, salt, null, (err, hash) => {
            if (err) {
                return next(err);
            }

            user.password = hash;
            next();
        });
    });
});

userSchema.methods.comparePassword = function(candidatePassword, callback) {
    bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
        if (err) {
            return callback(err);
        }

        callback(null, isMatch);
    });
};

export default mongoose.model('user', userSchema);
