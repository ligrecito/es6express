import mongoose from 'mongoose';

export const environmentSchema = new mongoose.Schema({
  name: String,
  _internalName: String,
  nodes: Number,
  provider: {
    name: String,
    nodes: Number,
    nodeSize: String,
    region: String
  },
  meta: {
    envProxyPort: Number, // used by downstream fabric api to communicate with this cluster
  }
});

let Environment = mongoose.model('environment', environmentSchema);

export default Environment;
