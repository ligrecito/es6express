import mongoose from 'mongoose';
import { environmentSchema } from './environments';

const organizationSchema = new mongoose.Schema({
    name: { type: String, unique: true },
    environments: [environmentSchema],
    created_at: Date,
    updated_at: Date
});

// on every save, add the date
organizationSchema.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();

  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});
// the schema is useless so far
// we need to create a model to use it
let Organization = mongoose.model('organization', organizationSchema);

Organization.save = (newOrg) => {
  return newOrg.save();
}
Organization.remove = (name) => {
  return Organization.remove({name});
}
Organization.findByName = (name) => {
  return Organization.findOne({ name });
}
Organization.getEnviroByName = (org, envName) => {
  //console.log(`Looking for environment: ${envName} for org:${org}...`);
  return org.environments.find(e => e.name === envName);
}
Organization.findAll = () => {
  return Organization.find();
}
Organization.addEnviro = (org, newEnv) => {
  // change the enviro name from: name to: org-name
  const mergedEnv =  Object.assign(newEnv, {_internalName: `${org.name}-${newEnv.name}`});
  console.log('mergedEnv:', mergedEnv);
  org.environments.push(mergedEnv);
  return org.save();
}

export default Organization;
