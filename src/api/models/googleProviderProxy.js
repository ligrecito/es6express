import mongoose from 'mongoose';

export const googleProviderProxySchema = new mongoose.Schema({
  orgName: String,
  managementPort: Number,
});

let GoogleProviderProxy = mongoose.model('googleProvider', googleProviderProxySchema);

GoogleProviderProxy.allocManagementPort = async(orgName) => {
  const totalAllocatedProxies = await GoogleProviderProxy.count({});
  const nextAllocatedProxy = 9000 + totalAllocatedProxies;
  let newGoogleProxy = GoogleProviderProxy({ orgName: orgName, managementPort: nextAllocatedProxy});
  await newGoogleProxy.save();
  return nextAllocatedProxy;
}

export default GoogleProviderProxy;
