import logger from '../../lib/logger';
import Organization from '../models/organizations';
import Environment from '../models/environments';
import { ResourceNotFoundError } from  '../../lib/custom-errors';
import * as GoogleProvider  from './providers/google.provider';

export const createOrganization = async(name) => {
  logger.debug('[organizations.js] createOrganization invoked');

  let newOrg = Organization({ name });
  await Organization.save(newOrg);
  logger.debug('Organization saved successfully');
}

export const listOrganizations = async() => {
  logger.debug('[organizations.js] listOrganizations invoked');
  const allOrgs = await Organization.findAll();
  return allOrgs;
}
