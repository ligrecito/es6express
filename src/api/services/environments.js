import logger from '../../lib/logger';
import Organization from '../models/organizations';
import Environment from '../models/environments';
import GoogleProviderProxy from '../models/googleProviderProxy';
import { ResourceNotFoundError } from  '../../lib/custom-errors';
import * as GoogleProvider  from './providers/google.provider';

export const addEnviroToOrg = async(providerName, orgName, envName, nodes, nodeSize, region) => {
  logger.debug('[organizations.js] addEnviroToOrg invoked');

  let org = await Organization.findByName(orgName);
  if (org) {
    // create a cluster in google
    const envProxyPort = await GoogleProviderProxy.allocManagementPort(orgName);
    logger.debug(`[organization.js] Allocated management port: ${envProxyPort}`);
    let clusterCreated = await GoogleProvider.createCluster(envProxyPort, org.name, `${org.name}-${envName}`, nodes, region);

    // persist metadata regarding that metadata in cplane db.
    let newEnv = Environment(
      { meta: {
          envProxyPort
        },
        provider: {
          name: providerName,
          region,
          nodes,
          nodeSize
        },
        name: envName
      });
    const orgUpdated = await Organization.addEnviro(org, newEnv);
    return orgUpdated;
  } else {
    logger.error('Organization not found');
    throw new ResourceNotFoundError({resource: 'organization', id: orgName });
  }
}

export const getClusterStatus = async(orgName, envName) => {
  logger.debug('[organizations.js] getClusterStatus invoked');
  let org = await Organization.findByName(orgName);
  if (org) {
    // Find the environment
    console.log('org=', org);
    const enviro = await Organization.getEnviroByName(org, envName);
    console.log('enviro=', enviro);
    if (enviro) {
      logger.debug(`Enquiring status for cluster: ${enviro._internalName}`)
      const status = await GoogleProvider.getClusterStatus(enviro._internalName);
      logger.debug(`Cluster Status: ${status.data}`)
      return status.data;
    } else {
      throw new ResourceNotFoundError({ resource: 'environment', id: envName });
    }
  } else {
    logger.error('Organization not found');
    throw new ResourceNotFoundError({resource: 'organization', id: orgName });
  }
}
