import axios from 'axios';
import logger from '../../lib/logger';
import config from '../../config/config.dev';
import { KongError } from  '../../lib/custom-errors';


/**
 * params:
 * - config = { name, hosts, uris, methods, upstream_url, https_only }
 */
export const deployApi = async(config) => {  //  => {
    try{
      const result = await axios.post(`${config.provider.kongUrl}`, config);
      return result.data;
    } catch (err) {
      logger.error(err.message);
      throw new Error(err.message);
    }
};

export const listAllApis = async() => {
  try{
    const result = await axios.get(`${config.provider.kongUrl}`);
    return result.data;
  } catch (err) {
    logger.error(err.message);
    console.log('throwing a KongError ...');
    throw new KongError(err.message);
  }
};

/**
 * params:
 * - name = (string) - the api to remove
 */
export const removeApi = async(name) => {
  try{
    const result = await axios.delete(`${config.provider.kongUrl}/${name}`);
    return result.data;
  } catch (err) {
    logger.error(err.message);
    throw new Error(err.message);
  }
}
