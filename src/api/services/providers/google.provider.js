import axios from 'axios';
import logger from '../../../lib/logger';
import config from '../../../config/config.dev';

export const createCluster = async(envProxyPort, orgName, clusterInternalName, numNodes, region) => {
  const createClusterReq = {
    meta: {
      proxyPort: envProxyPort
    },
    cluster: {
      environment: clusterInternalName,
      organization: orgName,
      nodes: numNodes,
      region: region
    }
  }
  try{
    const result = await axios.post(`${config.providers.googleUrl}/cluster`, createClusterReq);
    return result.data;
  } catch (err) {
    logger.error(err.message);
    throw new Error(err.message);
  }
}
/**
 * @param {string} clusterInternalName
 */
export const getClusterStatus = async(clusterInternalName) => {
  try {
    logger.debug(`[google.provider] Calling ${config.providers.googleUrl}/cluster/${clusterInternalName}/status ...`);
    const result = await axios.get(`${config.providers.googleUrl}/cluster/${clusterInternalName}/status`); // .../cluster/boral-dev/status?proxyPort=100000
    return result;
  } catch (err) {
    logger.error(err.message);
    throw new Error(err.message);
  }
}

export const createPodDeployment = async(envProxyPort, image, appName, envName) => {
  const deployPodRequest = {
    meta: {
      proxyPort: envProxyPort
    },
    pod: {
      image,
      appname: appName,
      namespace: envName 
    }
  }
  try{
    const result = await axios.post(`${config.providers.googleUrl}/pod`, deployPodRequest);
    return result.data;
  } catch (err) {
    logger.error(err.message);
    throw new Error(err.message);
  }
}
