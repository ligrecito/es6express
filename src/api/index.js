import { version } from '../../package.json';
import { Router } from 'express';
import {
	deploymentsApi,
	environmentsApi,
	healthApi,
	organizationsApi
} from './controllers';

var ApiRoutes = Router();

// mount the users resource
ApiRoutes.use('/health', healthApi);

// mount the deployment routes
ApiRoutes.use('/deployments', deploymentsApi);

ApiRoutes.use('/environments', environmentsApi);

// mount the organization routes
ApiRoutes.use('/organizations', organizationsApi);

// perhaps expose some API metadata at the root
ApiRoutes.get('/', (req, res) => {
	res.json({ version });
});

export default ApiRoutes;
