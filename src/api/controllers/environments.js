import { Router } from 'express';
import logger from '../../lib/logger';
import { ValidationError } from '../../lib/custom-errors';
import { wrapAsync, validateParams } from '../../lib/util';
import {
  addEnviroToOrg,
  getClusterStatus
} from '../services/environments';

export const environmentsApi = Router();

/**
 * Provision an environment within an organization
 * @param {string} orgName - Organization name
 * @param {Object} environment
 * @param {String} environment.name - Name given to the environment
 * @param {Number} environment.nodes - Number of vms to provision
 * @param {String} environment.region - Google region to provision
 */
environmentsApi.post(
  '/create',
  wrapAsync(async (req,res) => {
    logger.debug('Hit endpoint /api/environments/create');
    const provider = 'google';
    const orgName  = req.body.orgName;
    const envName  = req.body.environment.name;
    const nodes    = req.body.environment.nodes;
    const nodeSize = req.body.environment.nodeSize || 'default';
    const region   = req.body.environment.region;
    await addEnviroToOrg(provider, orgName, envName, nodes, nodeSize, region);
    res.json({ success: true });
  })
)

/**
 * /environment/dev/status?org=boral
 */
environmentsApi.get(
  '/:name/status',
    wrapAsync(async (req,res) => {
      const envName = req.params.name;
      const orgName = req.query.org;
      logger.debug(`Hit endpoint /api//environments/${envName}/create`);
      const output = await getClusterStatus(orgName, envName);
      res.json({ output });
    })
)
