import { Router } from 'express';
import { body, oneOf, validationResult } from 'express-validator/check';
import logger from '../../lib/logger';
import { ValidationError } from '../../lib/custom-errors';
import { wrapAsync, validateParams } from '../../lib/util';
//import deploymentService from '../services/deployments';
import * as GoogleProvider from '../services/providers/google.provider';
import Organization from '../models/organizations';

export const deploymentsApi = Router();

/**
 * Deploy a container into a given environment
 *  @param {string} name - application name. Resulting path: https://{baseUrl}/name
 *  @param {string} host - host for api gateway
 *  @param {string} uris - host to resolve at the api gateway
 *  @param {string} methods - allowed verbs by the api gateway
 *  @param {string} upstream_url - to be deleted
 *  @param {boolean} https_only -
 *
*/
deploymentsApi.post(
  '/create',
  wrapAsync(async (req,res) => {
    logger.debug('[controller.deployments] create deployment invoked...')
    const image    = req.body.image;
    const appName  = req.body.appName;
    const envName  = req.body.envName;
    const orgName  = req.body.orgName;

    logger.debug(`Looking for environment: ${envName} for organization: ${orgName}...`);
    const organization = await Organization.findByName(orgName);
    const enviro = Organization.getEnviroByName(organization, envName);
    const intEnviroName = enviro._internalName;
    const envProxyPort = enviro.meta.envProxyPort;
    logger.debug(`Deploying into environment: ${intEnviroName} via proxyPort:${envProxyPort} ...`);
    await GoogleProvider.createPodDeployment(envProxyPort, image, appName, intEnviroName);
    res.json({ success: true });
  })
);
// deploymentsApi.post('/create', [
//   body('name').exists(),
//   oneOf([
//     body('hosts').exists(),
//     body('uris').exists().withMessage('<uris> is a mandatory field'),
//     body('methods').exists()
//   ]),
//   body('upstream_url').exists(),
// ], wrapAsync(async (req,res)=> {
//     logger.info('Hit endpoint: /api/deployments/create ...');
//
//     // Validate paramaters
//     validateParams(req);
//
//     logger.debug('deploying api ...');
//     await deploymentService.deployApi({
//       name: req.body.name,
//       hosts: req.body.hosts,
//       uris: req.body.uris,
//       methods: req.body.methods,
//       upstream_url: req.body.upstream_url,
//       https_only: req.body.https_only || false,
//     });
//     res.json({ success: true });
// }));

/**
 * List of all deployed applications
*/
deploymentsApi.get('/list', wrapAsync(async(req,res) => {
  logger.info('Hit endpoint: /api/deployments/list ...');
  const result = await deploymentService.listAllApis();
  res.json({ output: result });
}));

/**
 * Undeploy a given application
*/
deploymentsApi.get('/delete/:name', wrapAsync(async(req,res)=> {
  logger.info('Hit endpoint: /api/deployments/delete ...');
  const apiName = req.params.name;
  const result = await deploymentService.removeApi(apiName);
  res.json({ output: result });
}));
