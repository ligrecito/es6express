import { Router } from 'express';
import logger from '../../lib/logger';
import { ValidationError } from '../../lib/custom-errors';
import { wrapAsync, validateParams } from '../../lib/util';
import {
  createOrganization,
  listOrganizations
} from '../services/organizations';

export const organizationsApi = Router();

/**
* Create an organization
* @param {string} name - organization name
*/
organizationsApi.post(
  '/create',
  wrapAsync(async (req,res) => {
    logger.debug('Hit endpoint: /api/organizations/create ...');
    const name = req.body.name;
    await createOrganization(name);
    res.json({ success: true });
  })
);

/**
 * List of all organizations
 *
 */
organizationsApi.get(
  '/list',
  wrapAsync(async (req,res) => {
    logger.debug('Hit endpoint: /api/organizations/list ...');
    const allOrgs = await listOrganizations();
    res.json({ data: allOrgs});
  })
);


// organizationsApi.post(
//   '/grant-access',
//   wrapAsync(async (req,res)=> {
//     logger.debug('Hit endpoint: /api/organizations/grant-access ...');
//     res.json({ success: true });
//   })
// );
