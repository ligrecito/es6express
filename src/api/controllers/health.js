import { Router } from 'express';
//import * as userService from './service';

export const healthApi = Router();

healthApi.get('/', (req, res) => {
  res.json({
    success: true,
    message: 'Service up and running'
  });
})
