import AuthMean from './model';
import jwt from 'jsonwebtoken';
import logger from '../../lib/logger';

export const register = async(req, res) => {
    var nick = new AuthMean({
      name: 'admin',
      password: 'admin',
      admin: true
    });
  	nick.save((err)=> {
      if (err) throw err;

      console.log('User saved successfully');
      res.json({ success: true });
    })
};


export const issueToken = async (req,res) => {
  try {
    logger.info('issueToken invoked');

    // find the user
    const user = await AuthMean.findOne({
      name: req.body.name
    });

    if (!user) {
      logger.error('user not found')
      res.json({success: false, message: 'Authentication failed'});
    } else {
      logger.debug(`checking passwords: ${req.body.password} and ${user.password} ...`);
      if (user.password !== req.body.password) {
        logger.error('user password does not match');
        res.json({ success: false, message: 'Authentication failed'});
      } else {
        // user exists and password is correct
        const payload = {
          admin: user.admin
        };

        let token = jwt.sign(payload, 'supersecret', {
          expiresIn: 24*60*60 //expires in 24 hours
        });

        res.json({
          success: true,
          message: 'Enjoy your token!',
          token: token
        });
      }
    }

  } catch(err){
    logger.error(`error looking for user, due to ${err}`);
    res.send('Got error while looking for user');
  }
}
