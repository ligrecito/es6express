import { Router } from 'express';
import * as authService from './service';

const api = Router();

api.get('/register', (req,res) => {
  authService.register(req,res);
});

api.post('/issue', (req,res) => {
  authService.issueToken(req,res);
});



export default api;
