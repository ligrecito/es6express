import mongoose, { Schema } from 'mongoose';

var AuthMean = mongoose.model('AuthMean', new Schema({
  name: String,
  password: String
}));

export default AuthMean;
