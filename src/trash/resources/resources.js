// import resource from 'resource-router-middleware';
// import resources from '../models/resources';
//
// export default ({ config, db }) => resource({
//
// 	/** Property name to store preloaded entity on `request`. */
// 	id : 'resource',
//
// 	/** For requests with an `id`, you can auto-load the entity.
// 	 *  Errors terminate the request, success sets `req[id] = data`.
// 	 */
// 	load(req, id, callback) {
// 		var resource = resources.find( resource => resource.id===id ),
// 			err = resource ? null : 'Not found';
// 		callback(err, resource);
// 	},
//
// 	/** GET / - List all entities */
// 	index({ params }, res) {
// 		res.json(resources);
// 	},
//
// 	/** POST / - Create a new entity */
// 	create({ body }, res) {
// 		body.id = resources.length.toString(36);
// 		resources.push(body);
// 		res.json(body);
// 	},
//
// 	/** GET /:id - Return a given entity */
// 	read({ resource }, res) {
// 		res.json(resource);
// 	},
//
// 	/** PUT /:id - Update a given entity */
// 	update({ resource, body }, res) {
// 		for (let key in body) {
// 			if (key!=='id') {
// 				resource[key] = body[key];
// 			}
// 		}
// 		res.sendStatus(204);
// 	},
//
// 	/** DELETE /:id - Delete a given entity */
// 	delete({ resource }, res) {
// 		resources.splice(resources.indexOf(resource), 1);
// 		res.sendStatus(204);
// 	}
// });
