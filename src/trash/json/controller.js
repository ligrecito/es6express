import { Router } from 'express';
import path from 'path';
import fs from 'fs';

const api = Router();


api.get('/vehicles', (req,res)=> {
  var vehiclesFilePath = path.join(__dirname, '/data/get-vehicles-response.json');
  const readable = fs.createReadStream(vehiclesFilePath);
  readable.pipe(res);
});

api.get('/orders', (req,res) => {
  // var ordersFilePath = path.join(__dirname, '/data/get-orders-response.json');
  // const readable = fs.createReadStream(ordersFilePath);
  // readable.pipe(res);
  res.sendFile(path.join(__dirname, '/data/get-orders-response.json'));
})

export default api;
