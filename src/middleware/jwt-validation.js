import jwt from 'jsonwebtoken';
import logger from '../lib/logger';

const jwtValidator = (req, res, next) => {
	var token = req.body.token || req.query.token || req.headers['authorization'];
	
	//decode token
	if (token) {
		logger.info('Verifying token...');
		jwt.verify(token, 'supersecret', (err, decoded) => {
			if (err){
				return res.json({ success: false, message: 'Failed to validate token.'});
			} else {
				req.decoded = decoded;
				next();
			}
		});
	} else {
		// if there is no token, return error
		logger.error('Token cannot be found');
		return res.status(403).send({
			success: false,
			message: 'No token provided.'
		});
	}
}

export default jwtValidator;
