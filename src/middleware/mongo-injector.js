import logger from '../lib/logger';
import monk from 'monk';

const mongoInjector = (req, res, next) => {
	logger.info('Injecting db ...');
	req.db = monk('mongodb://jteso:cdujteso#100@ds125198.mlab.com:25198/cplane);
	next();
}

export default mongoInjector;
