import makeError from 'make-error';

export const ValidationError = makeError('ValidationError');
export const KongError = makeError('KongError');
export const ResourceNotFoundError = makeError('ResourceNotFoundError');
