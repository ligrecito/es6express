
# API
## Organisations

- `/api/organizations/create`
- `/api/organizations/list`
- `/api/organizations/grant-access`

## Environments

- `/api/environments/create`


    ```
    POST :8089/api/cluster
    { environment:
      {
        name: 'dev',
        nodes: 3,
        region 'australia-southast1-a'
      }
    }
    ```

- `/api/environments /:name`

    ```
    GET /api/cluster/:name
    status: [STOPPED' |PROVISIONING | RUNNING]
    ```

- `/api/environments/list`
- `/api/environments/delete/:name`

## Deployments

- `/api/deployments/create`


- `/api/deployments/list`
- `/api/deployments/delete/:name`




# Schema

## Users
- username
- password
- tries
- blocked
- info: {
  - firstname
  - lastname
  - email
  - admin
  - organizationId
}

## Organization
- id
- name
- environments: [
  {
    name: 'dev',
    nodes: 3,
    region 'australia-southast1-a'
  },
  ...
]
- users: [ObjectId, ObjectId, ...]
